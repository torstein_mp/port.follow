import React from 'react';
import { render } from 'react-dom';
import App from "./App";
import {composeWithDevTools} from 'redux-devtools-extension'

import {HashRouter as Router} from "react-router-dom";
import  './style/bootstrap.min.css'
import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
import {Provider} from "react-redux";
import reducer from "./reducers";
import {signedIn} from "./AC/loginActions";

import * as firebase from "firebase";
import database from "./configs/firebaseConfig"


const store = createStore(
    reducer,
   composeWithDevTools(applyMiddleware(thunk))
);


const  unsubscribe = firebase.auth().onAuthStateChanged((authUser) => {

    if (authUser != null) {
        localStorage.setItem('authUser', JSON.stringify(
            {
                token: authUser.refreshToken,
                userData: {providerData: [authUser.providerData[0]]}
            }));
       return  database.collection('users').doc(authUser.uid).get()
            .then((doc)=>{
             return  store.dispatch(signedIn(authUser, authUser.refreshToken,doc.data()))
            })
    } else {
        localStorage.removeItem('authUser');
    }
}, () => {
    localStorage.removeItem('authUser');

    unsubscribe();
});

render(
    <Provider store={store}>
    <Router>
        <App />
    </Router>
    </Provider>
, document.getElementById('root'));
