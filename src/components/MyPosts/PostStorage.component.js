import React, {Component, Fragment} from 'react';
import api from "../../api";
import HomeItem from "../Home/HomeItem";
import {connect} from "react-redux";
import Modal from "../Overall/Modal/Modal";

class PostStorage extends Component {

    state = {
        fetchStatus: "pending",
        posts: [],
        showModal: false,
        targetPost: '',
    };

    componentDidMount() {
        api.posts.getAllPosts(this.props.uid)
            .then(payload => {
                //console.log(payload);
                this.setState(payload);
            });
    }

    deleteTargetPost = () => {
        api.posts.deletePost(this.state.targetPost)
            .then(() => {
                return api.posts.getAllPosts(this.props.uid)
            })
            .then(payload => {
                this.setState(payload, () => this.handleToggleModal());
            })
    };


    handleToggleModal = (id) => {
        return this.setState({showModal: !this.state.showModal, targetPost: (id || '')})
    };

    render() {
        //console.log(this.state.posts)
        const {fetchStatus, posts} = this.state;
        return (
            <Fragment>
                {this.state.showModal &&
                <Modal onCloseRequest={this.handleToggleModal}>
                    <p className='hero'>You want delete this post ?</p>
                    <br/>
                    <div className='row justify-content-around'>
                        <button className='btn btn-danger col-4' onClick={this.deleteTargetPost}> Yes</button>
                        <button className='btn btn-success col-4' onClick={this.handleToggleModal}> No</button>
                    </div>
                </Modal>
                }
            <main>
                <div className="jumbotron">
                    <div className="row justify-content-center">
                        <div className="col-6 hero">
                            <h2>My posts</h2>
                        </div>
                    </div>
                </div>
                <div>
                    {fetchStatus === "success" ?
                        <div className='container'>
                        <div className="sectionLayout--insetColumn row justify-content-center" style={{margin:'5% auto'}}>
                            {/*<HomeItem description={'New post to tell your story'}*/}
                            {/*          title={"Add post"}*/}
                            {/*          newPost*/}
                            {/*          history={this.props.history}/>*/}
                            {posts.length >= 1 && posts.map((item) => {
                                console.log(item)
                               return <HomeItem key={item.id}
                                          id={item.id}
                                          img={item.about.img}
                                          history={this.props.history}
                                          description={item.about.descr}
                                          title={item.about.title}
                                          postTime={item.about.postTime.toDate()}
                                          userData={item.userData}
                                          toggleModal={this.handleToggleModal.bind(this)}
                                          editable
                                />
                            })}
                        </div>
                        </div>
                        : <div className='spinner'>
                            <wired-spinner  spinning duration="1000"/>
                        </div>}
                </div>
                <br/>
            </main>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    uid: state.login.user.uid
});
export default connect(mapStateToProps)(PostStorage);