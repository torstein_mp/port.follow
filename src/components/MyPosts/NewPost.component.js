import React, {Component} from 'react';
import {EditorState, convertToRaw, ContentState} from 'draft-js';
import {Editor} from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

import * as firebase from "firebase";
import api from "../../api";
import {connect} from "react-redux";
import Modal from "../Overall/Modal/Modal";
import SavePost from "./modalSavePost";
import database from "../../configs/firebaseConfig";

class NewPost extends Component {

    constructor(props) {
        super(props);
        this.images = [];
        this.posted = false;
        const html = sessionStorage.getItem('post') || '';
        const contentBlock = htmlToDraft(html);
        if (contentBlock) {
            const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
            const editorState = EditorState.createWithContent(contentState);
            this.state = {
                editorState,
                showModalSave: false,
                description: {},
                postTextLength: 0,
                docId: sessionStorage.getItem('docId') || ''

            };
        }

    }

    componentDidMount() {
        if (!this.state.docId) {
            const newDoc = database.collection('usersPosts').doc().id;
            sessionStorage.setItem('docId', newDoc);
            this.setState({docId: newDoc})
        }
    }


    componentDidUpdate(prevProps, prevState) {
        const postTextLength = document.querySelector('.sectionLayout--insetColumn').innerText.length;
        if (prevState.postTextLength !== postTextLength) {
            this.setState({postTextLength})
        }
    }

    componentWillUnmount() {
        sessionStorage.clear();
        if (!this.posted) {
            this.deleteFilesFromStore();
        }
    }

    handleToggleModal = () => {
        return this.setState({showModalSave: !this.state.showModalSave})
    };
    getPostDescription = () => {
        const post = document.querySelector('.sectionLayout--insetColumn');
        const img = post.querySelector('img');
        const description = {
            title: post.querySelector('span').innerText,
            titleImg: (img ? img.src : "")
        };
        this.setState({description}, () => this.handleToggleModal())
    };

    uploadPostChanges = (descr, title, img) => {
        const {uid} = this.props;
        const {docId} = this.state;
        const body = draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()));
        const payload = {
            title: title,
            descr: descr,
            img: img,
            body: body
        };
        api.posts.addNewPost(uid, payload, docId)
            .then(() => {
                this.posted = true;
                this.handleToggleModal();
                this.props.history.push('myPosts')
            });
    };

    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
        }, () => sessionStorage.setItem("post", draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()))));
    };

    uploadImageCallBack = (file) => {
        const storageRef = firebase.storage().ref().child(`postsImages/${this.state.docId}/${file.name}`);
        this.images.push(file.name);
        return storageRef.put(file)
            .then(function (snapshot) {
                return snapshot.ref.getDownloadURL()
                    .then(function (downloadURL) {
                        console.log("File available at", downloadURL);
                        return {data: {link: downloadURL}};
                    });
            });
    };

    deleteFilesFromStore = () => {
        this.images.forEach((item) => {
            const storageRef = firebase.storage().ref().child(`postsImages/${this.state.docId}/${item}`);
            return storageRef.delete()
                .then(() => console.log('deleted'))
                .catch((er) => {
                    console.log(er.message)
                })
        });

    };

    render() {
        const {showModalSave, description} = this.state;
        return (
            <main>
                {showModalSave &&
                <Modal onCloseRequest={this.handleToggleModal.bind(this)}>
                    <SavePost closeModal={this.handleToggleModal.bind(this)}
                              description={description}
                              uploadPost={this.uploadPostChanges.bind(this)}/>
                </Modal>}
                <br/>
                <div className='postArticle container'>
                    <button className="btn btn-success"
                            onClick={this.getPostDescription}
                            disabled={this.state.postTextLength <= 100 ? 'disabled' : ''}>Ready to post?
                    </button>
                    <div className='section-content'>
                        <Editor
                            editorState={this.state.editorState}
                            toolbarClassName="sectionLayout--toolBar"
                            wrapperClassName="wrapperClassName"
                            editorClassName="sectionLayout--insetColumn"
                            onEditorStateChange={this.onEditorStateChange}
                            toolbar={{
                                image: {
                                    alignmentEnabled: false,
                                    uploadCallback: this.uploadImageCallBack,
                                    previewImage: true,
                                    alt: {present: true, mandatory: false},
                                    defaultSize: {
                                        height: '100%',
                                        width: '100%',
                                    }
                                }
                            }}
                        />
                    </div>
                </div>
            </main>
        );
    }
}

const mapStateToProps = (state) => ({
    uid: state.login.user.uid,
});

export default connect(mapStateToProps)(NewPost);