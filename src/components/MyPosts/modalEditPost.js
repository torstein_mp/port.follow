import React,{useState} from 'react'
import noImg from '../../img/no-photo.png'


const ModalEditPost = (props) => {
    const [descr,setDescr] = useState('');
    const {description} = props;
    return(
        <div>
            <p className='exitIcon'><button className="btn btn-outline-dark" onClick={props.closeModal}>X</button></p>
            <span className='hero'><h3>{description.title}</h3></span>
            <p><img src={description.titleImg || noImg} alt=''  className='newPostModal'/></p>
            <p>
                <label htmlFor='description'>Description*</label>
                <textarea name='description'
                          className='form-control'
                          placeholder="Write your new post`s preview...."
                          style={{resize:'none'}}
                          onChange={({target})=>setDescr(target.value)}/>
            </p>
            <button className="btn btn-success "
                    onClick={props.uploadPost.bind(null,descr,description.title,description.titleImg)}
            disabled={descr.length ? '' : 'disabled' }>Edit your story</button>
        </div>
    )
};

export default ModalEditPost