import React, {Component} from 'react';
import htmlToDraft from "html-to-draftjs";
import {ContentState, convertToRaw, EditorState} from "draft-js";
import draftToHtml from "draftjs-to-html";
import {Editor} from "react-draft-wysiwyg";
import * as firebase from "./NewPost.component";
import api from "../../api";
import Modal from "../Overall/Modal/Modal";
import ModalEditPost from "./modalEditPost";

class EditPost extends Component {

    constructor(props) {
        super(props);
        this.posted = false;
        this.images= [];
           const  html = sessionStorage.getItem('post') || "";
            const editorState = this.makeEditorState(html);
            this.state = {
                fetchStatus:"pending",
                editorState,
                description:{},
                showModalSave: false,
                docId: this.props.match.params.id
            };

    }
    componentWillUnmount() {
        sessionStorage.clear();
        if (!this.posted){
            this.deleteFilesFromStore();
        }
    }
    componentDidMount() {
        console.log("MOUNT")
        if (!sessionStorage.getItem('post')) {
            api.posts.getPostById(decodeURIComponent(this.props.match.params.id))
                .then((payload) => {
                    sessionStorage.setItem('post', payload.post);
                    const editorState = this.makeEditorState(payload.post);
                    return this.setState({fetchStatus: payload.fetchStatus, editorState})
                })
        }
        else {
            this.setState({fetchStatus:'success'})
        }
    }
    uploadPostChanges = (descr,title,img) => {
        const {docId} = this.state;
        const body = draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()));
        const payload = {
            title:title,
            descr:descr,
            img:img,
            body:body
        };
        api.posts.editPost(payload,docId)
            .then(()=>{
                this.posted = true;
                this.handleToggleModal();
            });
    };
    handleToggleModal = () => {
        return this.setState({showModalSave:!this.state.showModalSave})
    };
    makeEditorState = (html) =>{
        const contentBlock = htmlToDraft(html);
        const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
        return  EditorState.createWithContent(contentState);

    }
    onEditorStateChange = (editorState) => {
        this.posted = false;
        this.setState({
            editorState,
        }, () => sessionStorage.setItem("post", draftToHtml(convertToRaw(this.state.editorState.getCurrentContent()))));
    };
    getPostDescription = () => {
        const post = document.querySelector('.sectionLayout--insetColumn');
        const description = {
            title: post.querySelector('span').innerText,
            titleImg: post.querySelector('img').src
        };
        this.setState({description}, () => this.handleToggleModal())
    };
    uploadImageCallBack = (file) => {
        const storageRef = firebase.storage().ref().child(`postsImages/${this.state.docId}/${file.name}`);
        this.images.push(file.name);
        return storageRef.put(file)
            .then(function (snapshot) {
                return snapshot.ref.getDownloadURL()
                    .then(function (downloadURL) {
                        return {data: {link: downloadURL}};
                    });
            });
    };
    deleteFilesFromStore = () => {
        this.images.forEach((item)=>{
            const storageRef = firebase.storage().ref().child(`postsImages/${this.state.docId}/${item}`);
            return storageRef.delete()
                .then(()=> console.log('deleted'))
                .catch((er) => {
                    console.log(er.message)
                })
        });

    };

    render() {
        const {showModalSave,description} = this.state;
        return (
            <main>
                {showModalSave &&
                <Modal onCloseRequest={this.handleToggleModal}>
                    <ModalEditPost closeModal={this.handleToggleModal}
                              description={description}
                              uploadPost={this.uploadPostChanges}/>
                </Modal>}
                {this.state.fetchStatus === 'success'
                    ?

                    <div className='postArticle container'>
                        <br/>
                        <button className="btn btn-success" onClick={this.getPostDescription}>Save changes</button>
                        <span style={{marginLeft:'1%',fontWeight:'700'}} > {this.posted ? 'Saved' : "Not saved"}</span>
                        <div className='section-content'>
                            <Editor
                                editorState={this.state.editorState}
                                toolbarClassName="sectionLayout--toolBar"
                                wrapperClassName="wrapperClassName"
                                editorClassName="sectionLayout--insetColumn"
                                onEditorStateChange={this.onEditorStateChange}
                                toolbar={{
                                    image: {
                                        alignmentEnabled: false,
                                        uploadCallback: this.uploadImageCallBack,
                                        previewImage: true,
                                        alt: {present: true, mandatory: false},
                                        defaultSize: {
                                            height: '100%',
                                            width: '100%',
                                        }
                                    }
                                }}
                            />
                        </div>
                    </div>
                    :
                    <div className='spinner'>
                        <wired-spinner  spinning duration="1000"/>
                    </div>
                }
            </main>
        );
    }
}

export default EditPost;