import React, {useEffect, useState} from 'react';
import './SettingsComponent.scss'
import {connect} from "react-redux";
import api from "../../api";
import {updateProfile} from "../../AC/loginActions";
import noImg from "../../img/no-photo.png";

const ProfileSettings = (props) => {
    const {user, userDB} = props;
    return (
        <main className="option">
            <div className="option-hero">
                <h3>PROFILE</h3>
            </div>
            <div className="option-body">
            <div>
                <div className='cont'>
                    <label htmlFor='img' style={{display:'contents',cursor:'pointer'}}>
                    <img
                        alt='Card'
                        className="avatar"
                        src={userDB.img || noImg}
                        title="avatar"
                    />
                        <div className='overlay'>Change</div>
                    </label>
                </div>
            </div>
                <input type='file'  id='img' style={{display:'none'}} onChange={(file)=>api.settings.updatePhoto(file.target.files[0],user.uid)}/>
                <div>
                    <label htmlFor="displayName">Display name:</label>
                    <input name="displayName" className="form-control"
                           defaultValue={userDB.name}/>
                </div>
                <br/>
                <h4 className='hero'>About:</h4>
                <p>{userDB.about || "No specialised info"}</p>

            </div>
        </main>
    );

};
const mapStateToProps = (state) => ({
    user: state.login.user,
    userDB: state.login.userDB,
    token: state.login.userId
});

export default connect(mapStateToProps, {updateProfile})(ProfileSettings);