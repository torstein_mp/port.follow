import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './SettingsComponent.scss'
import AuthWrap from "../PrivateRoutes/AuthWrap.container";
import { NavLink, Switch} from "react-router-dom";
import ProfileSettings from "./ProfileSettings";
import PasswordSettings from "./PasswordSettings";

class Settings extends Component {
    render() {
        return (
            <main className="settings-page" >
                <div className="jumbotron">
                    <div className="row justify-content-center">
                        <div className="col-6 hero">
                    <h2>Settings</h2>
                        </div>
                    </div>
                </div>
                <div className="container">
                <div className="row justify-content-center">

                    <div className="col-3 list-group">
                        <NavLink to='/settings/profile' type="button" className="list-group-item list-group-item-action ">
                            SET UP PROFILE
                        </NavLink>
                        <NavLink to='/settings/changePassword' type="button" className="list-group-item list-group-item-action">
                            Change Password
                        </NavLink>
                        <NavLink to='/settings/changeEmail' type="button" className="list-group-item list-group-item-action">
                            Change Email
                        </NavLink>

                    </div>

                    <div className="col-7 settingsBlock" >
                        <Switch>
                        <AuthWrap exact path="/settings/profile" component={ProfileSettings}/>
                        <AuthWrap exact path="/settings/changePassword" component={PasswordSettings}/>
                        <AuthWrap exact path="/settings/changeEmail"/>
                        </Switch>
                    </div>
                </div>
                </div>
            </main>
        );
    }
}


Settings.propTypes = {};

export default  Settings;
