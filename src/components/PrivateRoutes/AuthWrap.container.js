import {connect} from "react-redux";
import AuthWrap from "./AuthWrap.component";


const mapStateToProps = (state) =>({
    auth:state.login.userId
});

export default connect(mapStateToProps)(AuthWrap)