import React, {Component} from 'react'
import {Redirect, Route} from "react-router-dom";

class AuthWrap extends Component{

    render() {
        const { auth, exact, path, component } = this.props;
        return auth ? (
            <Route exact={exact ? exact : false} path={path} component={component}/>
        ) : (
            <Redirect to="/"/>
        );
    }
}

export default AuthWrap;