import React from 'react'
import {Link} from "react-router-dom";

const FeaturedPost = (props) =>{
    const fixedEncodeURIComponent = (str) => {
        return encodeURIComponent(str).replace(/[!'()*]/g, function (c) {
            return '%' + c.charCodeAt(0).toString(16);
        });
    };
    const timeStampToDate = (timeStamp) => {
        const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        const day = timeStamp.getDate();
        const month = monthNames[timeStamp.getMonth()];
        return ` ${month} ${day}`
    };
  return(
      <div className='featuredPost'>
          <div className='featuredPost-img'>
              <Link to={`post/${fixedEncodeURIComponent(props.id)}`}>
                  <img src={props.img} style={{width:'100%'}} alt/>
              </Link>
          </div>
          <div className='featuredPost-text'>
              <h2>{props.title}</h2>
              <p>{props.description}</p>
          </div>
          <div className='featuredPost-info row'>
              <img src={props.userData.img} className='cardIcon' alt/>
              <div className='flex-column'>
              <span>{props.userData.name}</span>
                  <span style={{
                      color: 'grey',
                      fontStyle: 'italic',
                      display:'block'
                  }}> {timeStampToDate(props.postTime)}</span>
              </div>
          </div>
      </div>
  )
};

export default FeaturedPost;