import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import HomeItem from "./HomeItem";
import api from "../../api";
import FeaturedPost from "./FeaturedPost";

class HomeComponent extends Component {
    state = {
        fetchStatus: "pending",
        posts: [],
    };

    componentDidMount() {
        api.homePage.getAllPortfolios().then(payload => {
            this.setState(payload);
        });
    }

    render() {
        const {fetchStatus, posts} = this.state;
        return (
            <Fragment>
                <main className='container'>
                        {fetchStatus === "success" ?
                            <div className="sectionLayout--insetColumn row justify-content-center" style={{margin:'5% auto'}}>
                                {posts.filter((item,index)=>index === 0).map((item)=>(
                                    <FeaturedPost
                                        key={item.id}
                                        id={item.id}
                                        img={item.about.img}
                                        history={this.props.history}
                                        description={item.about.descr}
                                        title={item.about.title}
                                        postTime={item.about.postTime.toDate()}
                                        userData={item.userData}
                                    />
                                ))}
                                <br/>
                                <h3>Latest</h3>
                                <div className='divider'/>
                                {posts.filter((item,index)=>index > 0).map((item, index) => (
                                    <HomeItem key={item.id}
                                              id={item.id}
                                              img={item.about.img}
                                              history={this.props.history}
                                              description={item.about.descr}
                                              title={item.about.title}
                                              postTime={item.about.postTime.toDate()}
                                              userData={item.userData}
                                    />
                                ))}
                            </div>
                            : <div className='spinner'>
                                <wired-spinner spinning duration="1000"/>
                            </div>}
                    <br/>
                </main>
            </Fragment>
        );
    }
}


export default HomeComponent;
