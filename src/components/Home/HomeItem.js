import React from 'react';
import noImg from '../../img/no-photo.png'
import {Link} from "react-router-dom";


const HomeItem = (props) => {
    const truncate = (text) => {
        if (text.length > 5)
            return text.substring(0, 120) + '...';
        else
            return text;
    };
    const openCreateWindow = () => {
        return props.history.push('/new-post')
    };
    const openPost = () =>{
        return props.history.push(`post/${fixedEncodeURIComponent(props.id)}`)
    };
    const editPost = () =>{
      return props.history.push(`edit-post/${fixedEncodeURIComponent(props.id)}`)
    };
    const fixedEncodeURIComponent = (str) => {
        return encodeURIComponent(str).replace(/[!'()*]/g, function (c) {
            return '%' + c.charCodeAt(0).toString(16);
        });
    };
    const timeStampToDate = (timeStamp) => {
        const monthNames = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"
        ];
        const day = timeStamp.getDate();
        const month = monthNames[timeStamp.getMonth()];
        return ` ${month} ${day}`
    };
    return (
        <div className='col-12'>
            {props.newPost ?
                <div className='newCard card' onClick={openCreateWindow}>
                    <img
                        alt='Card'
                        className="card-img-top"
                        src={noImg}
                        title="Contemplative Reptile"
                    />
                    <div className='card-body'>
                        <h5 className='card-title'>
                            {props.title}
                        </h5>
                        <p className='card-text'>
                            {props.description}
                        </p>
                    </div>
                </div>
                : props.editable ?
                    <section className='row justify-content-between'>
                            <div className='post col-8'>
                                <h5 className='card-title'>
                                    <Link to={`post/${fixedEncodeURIComponent(props.id)}`}> {props.title}</Link>
                                </h5>
                                <span className='card-text'>
                                <Link to={`post/${fixedEncodeURIComponent(props.id)}`}>
                                    {truncate(props.description)}</Link>
                                <br/>
                                <p style={{
                                    color: 'grey',
                                    fontStyle: 'italic',
                                    marginTop: "5%"
                                }}>
                                    {props.userData.name} • {timeStampToDate(props.postTime)}</p>
                            </span>
                            </div>
                            <div className='col-3'>
                                <Link to={`post/${fixedEncodeURIComponent(props.id)}`}>
                                    <img
                                        alt='Card'
                                        className="card-img-top"
                                        src={props.img || noImg}
                                        title="img"
                                    />
                                </Link>
                            </div>
                            <div className='row justify-content-around'>
                                <button onClick={editPost}
                                      className="btn btn-warning col-3">Edit</button>
                                <button onClick={props.toggleModal.bind(null, props.id)}
                                        className="btn btn-danger col-3">Delete
                                </button>

                            </div>
                    </section>
                    :
                    <section className='row justify-content-between'>
                        <div className='post col-8'>
                            <h5 className='card-title'>
                                <Link to={`post/${fixedEncodeURIComponent(props.id)}`}> {props.title}</Link>
                            </h5>
                            <span className='card-text'>
                                <Link to={`post/${fixedEncodeURIComponent(props.id)}`}>
                                    {truncate(props.description)}</Link>
                                <br/>
                                <p style={{
                                    color: 'grey',
                                    fontStyle: 'italic',
                                    marginTop: "5%"
                                }}>
                                    {props.userData.name} • {timeStampToDate(props.postTime)}</p>
                            </span>
                        </div>
                        <div className='col-3'>
                            <Link to={`post/${fixedEncodeURIComponent(props.id)}`}>
                            <img
                                alt='Card'
                                className="card-img-top"
                                src={props.img || noImg}
                                title="img"
                            />
                            </Link>
                        </div>
                    </section>
            }

        </div>
    );
};


export default HomeItem;