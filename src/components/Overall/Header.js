import React, {Component, Fragment} from 'react';
import {Link} from "react-router-dom";
import Modal from "./Modal/Modal";
import SignIn from "../Login/SignIn";
import SignUp from "../Login/SignUp";
import {connect} from "react-redux";
import {signOut} from "../../AC/loginActions";


class Header extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showModalLogin: false,
            showModalRegistration: false,
        };
    }

    handleToggleModal(target) {
        switch (target) {
            case "login":
                return this.setState({showModalLogin: !this.state.showModalLogin});
            case "registration":
                return this.setState({showModalRegistration: !this.state.showModalRegistration});
            default :
                return console.log("error with modal window")
        }
    }

    render() {
        const {showModalLogin,showModalRegistration} = this.state;
        const {userId} = this.props;
        return (
                <header>
                    {showModalLogin &&
                    <Modal onCloseRequest={this.handleToggleModal.bind(this, 'login')}>
                        <SignIn
                                closeModal={this.handleToggleModal.bind(this, "login")}/>
                    </Modal>}
                    {showModalRegistration &&
                    <Modal onCloseRequest={this.handleToggleModal.bind(this, 'registration')}>
                        <SignUp
                            closeModal={this.handleToggleModal.bind(this, "registration")}/>
                    </Modal>}

                    {(userId) ?
                        <nav className="navbar navbar-dark bg-secondary">
                            <Link to="/" className="navbar-brand"><h3>Port.Follow</h3></Link>
                            <div className="row">
                                <Link to="/myPosts" className="text-white col-7">
                                    <h5 className="text-white">My Posts</h5></Link>
                                <Link to="/settings/profile" className="text-white col-4"> <h5>Settings</h5></Link>
                            </div>
                            <div className=''>
                                <div className="btn btn-light"
                                     style={{marginRight: "15px"}}>
                                    {this.props.user.name }
                                </div>
                                <button type="button"
                                        className="btn btn-outline-light"
                                        onClick={this.props.signOut}>Logout</button>
                            </div>
                        </nav>
                        :
                        <nav className="navbar navbar-dark bg-secondary">
                            <Link to="/" className="navbar-brand"><h3>Port.Follow</h3></Link>

                            <div>
                                <button type="button"
                                        className="btn btn-outline-light"
                                        style={{marginRight: "15px"}}
                                        onClick={this.handleToggleModal.bind(this, 'login')}>Login
                                </button>
                                <button type="button"
                                        className="btn btn-light"
                                        onClick={this.handleToggleModal.bind(this, 'registration')}>Sign up</button>
                            </div>
                        </nav>
                        }

                </header>
        );
    }
}
function mapStateToProps(state) {
    return {
        userId:state.login.userId,
        user:state.login.userDB
    };
}
export default connect(mapStateToProps,{
    signOut
})(Header);