import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Styles from "./Styles"
import injectSheet from 'react-jss'

class Modal extends Component {

    constructor(props) {
        super(props);

        this.handleKeyUp = this.handleKeyUp.bind(this);
        this.handleOutsideClick = this.handleOutsideClick.bind(this);
    }

    componentDidMount() {
        window.addEventListener('keyup', this.handleKeyUp, false);
        document.addEventListener('click', this.handleOutsideClick, false);
    }

    // Remove listeners immediately before a component is unmounted and destroyed.
    componentWillUnmount() {
        window.removeEventListener('keyup', this.handleKeyUp, false);
        document.removeEventListener('click', this.handleOutsideClick, false);
    }

    handleKeyUp(e) {
        const { onCloseRequest } = this.props;
        const keys = {
            27: () => {
                e.preventDefault();
                onCloseRequest();
                window.removeEventListener('keyup', this.handleKeyUp, false);
            },
        };

        if (keys[e.keyCode]) { keys[e.keyCode](); }
    }
    handleOutsideClick(e) {
        const { onCloseRequest } = this.props;

        if (this.modal) {
            if (!this.modal.contains(e.target)) {
                onCloseRequest();
                document.removeEventListener('click', this.handleOutsideClick, false);
            }
        }
    }

    render() {
        const {
            children,
            classes,
        } = this.props;

        return (
            <div className={classes.modalOverlay}>
                <div
                    className={classes.modal}
                    ref={node => (this.modal = node)}
                >
                    <div>
                        <div className={`container ${classes.containerStyle}`} >
                        {children}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

Modal.propTypes = {};

export default injectSheet(Styles) (Modal);
