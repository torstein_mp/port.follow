import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import {signInUser} from "../../AC/loginActions";

 const SignIn = (props) => {
     const [loginData,setLoginData] = useState({login:'',password:''});
     const {signInUser,closeModal} = props;
    useEffect(()=>{
        window.addEventListener('keyup', handleKeyUp, false);
        return ()=>{
            window.removeEventListener('keyup',handleKeyUp, false);
        }
    });

    const loginUser = (login,password) =>{
        signInUser(login,password);
        closeModal()
    };
    const handleKeyUp = (e) => {
         const keys = {
             13: () => {
                 e.preventDefault();
                 loginUser(loginData.login,loginData.password);
                 window.removeEventListener('keyup',handleKeyUp, false);
             },
         };

         if (keys[e.keyCode]) { keys[e.keyCode](); }
     };
    return(
      <div>
              <p><button className="btn btn-outline-dark" onClick={closeModal}>X</button></p>
              <label htmlFor="login"> Email</label>
              <input name="login" className="form-control" onChange={({target:{value}})=> setLoginData({...loginData,login: value})}/>
              <label htmlFor="password"> Password</label>
              <input name="password"
                     type="password"
                     className="form-control"
                     value={loginData.password}
                     onChange={({target:{value}})=> setLoginData({...loginData,password: value})}
              />
              <br/>
              <button className="btn btn-primary" onClick={loginUser.bind(null,loginData.login,loginData.password)}>Login</button>
      </div>
  )
};


export default connect(null,
    {signInUser}) (SignIn);
