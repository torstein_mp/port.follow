import React,{useState} from 'react'
import api from "../../api";

const SignUp = ({closeModal}) =>{
    const [loginData,setLoginData] = useState({login:'',password:'',username:''});
    const signUpUser = (login,password,name) => {
        api.authentication.signUp(login,password,name);
        closeModal()
    };
  return(
      <div>
          <p><button className="btn btn-outline-dark" onClick={closeModal}>X</button></p>
          <label htmlFor="name">Name</label>
          <input name="login" className='form-control' onChange={({target:{value}})=>setLoginData({...loginData,username: value})}/>
          <label htmlFor="login"> Email</label>
          <input name="login" className="form-control" onChange={({target:{value}})=> setLoginData({...loginData,login: value})}/>
          <label htmlFor="password"> Password</label>
          <input name="password"
                 type="password"
                 className="form-control"
                 value={loginData.password}
                 onChange={({target:{value}})=> setLoginData({...loginData,password: value})}
          />
          <br/>
          <button className="btn btn-primary"
                  onClick={signUpUser.bind(null,loginData.login,loginData.password,loginData.username)}>Sign up</button>
      </div>
  )
};

export default SignUp;