import React, {Component} from 'react';
import api from "../api";

class ViewPost extends Component {

    componentDidMount() {
        api.posts.getPostById(decodeURIComponent(this.props.match.params.id))
            .then((payload)=>this.setState(payload))
    }

    constructor(props)
    {
        super(props);
        this.state = {
            post:"",
            fetchStatus:"pending"
        }
    }
    render() {
        return (
            <main className='viewArticle container'>
                {this.state.fetchStatus === 'success' ?
                    <div className='section-content'>
                        <div className='sectionLayout--insetColumn' dangerouslySetInnerHTML={{__html:this.state.post}}/>
                    </div>
                    :
                    <div className='spinner'>
                        <wired-spinner  spinning duration="1000"/>
                    </div>
                }
            </main>
        );
    }
}

export default ViewPost;