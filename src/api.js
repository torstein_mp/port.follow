import * as firebase from "firebase"
import database from "./configs/firebaseConfig"


export default {
    homePage: {
        getAllPortfolios: () => {
            return database.collection('usersPosts')
                .where('about.descr', '>', '1')
                .get()
                .then((snap) => {
                    // console.log(snap.docs);
                    const db = snap.docs.map((doc) => {
                        let data = {};
                        return doc.data().userRef.get()
                            .then((res) => {
                                data.about = doc.data().about;
                                data.id = doc.id;
                                data.userData = res.data();
                                return data;
                            });
                    })
                    return  Promise.all(db).then((data)=> {
                        //console.log(data)
                        const payload = {};
                        payload.fetchStatus = "pending";
                        payload.posts = data;
                        return payload;
                    }).then((data)=> {
                        data.fetchStatus = 'success';
                        return data} )
                    //console.log(prom)
                })
        }
    },
    authentication: {
        signIn: (email, password) => {
            return firebase.auth().signInWithEmailAndPassword(email, password)
                .then((user)=>{
                    return database.collection('users').doc(user.user.uid)
                        .get()
                        .then((doc)=>{
                            const payload ={};
                            payload.user = user.user;
                            payload.id = user.user.refreshToken;
                            payload.dataStore = doc.data();
                            return payload;
                        })
                })
                .catch((error) => {
                    console.log(error.code);
                    console.log(error.message)
                })
        },
        signUp: (email, password,username) => {
            return firebase.auth().createUserWithEmailAndPassword(email, password)
                .then((user)=>{
                    return database.collection('users').doc(`${user.user.uid}`).set({
                        about:'',
                        img:'',
                        name:username,
                    })
                        .catch((er)=>{
                            return console.log(er.message)
                        })
                })
                .catch((error) => {
                    console.log(error.code);
                    console.log(error.message)
                })

        },
        signOut: () => {
            return firebase.auth().signOut()
                .catch((error) => {
                    console.log(error.code);
                    console.log(error.message)
                })
        }
    },
    settings: {
        updatePhoto: (file,uid) => {
            console.log(file)
            const storageRef = firebase.storage().ref().child(`usersImages/${uid}`);
            return storageRef.put(file)
                .then(function (snapshot) {
                    return snapshot.ref.getDownloadURL()
                        .then(function (downloadURL) {
                           return  database.collection('users').doc(uid).update({
                                img:downloadURL,
                            }).catch((er)=> console.log(er))
                        })
                });
        }
    },
    posts: {
        getPostById: (id) => {
            return database.collection('usersPosts')
                .doc(`${id} [body]`)
                .get()
                .then((snap)=>{
                    console.log(snap);
                    const data = snap.data().body;
                    const payload = {};
                    payload.fetchStatus = "success";
                    payload.post = data;
                    return payload;
                })
                .catch((error) => {
                    console.log(error.code);
                    console.log(error.message)
                })
        },
        getAllPosts: (uid) => {
            const docRef = firebase.firestore()
                .collection('users')
                .doc(`${uid}`);
            return database.collection('usersPosts')
                .where('userRef', '==', docRef)
                .where('about.descr', '>', '1')
                .get()
                .then((snap) => {
                    // console.log(snap.docs);
                    const db = snap.docs.map((doc) => {
                        let data = {};
                        return doc.data().userRef.get()
                            .then((res) => {
                                data.about = doc.data().about;
                                data.id = doc.id;
                                data.userData = res.data();
                                return data;
                            });
                    })
                   return  Promise.all(db).then((data)=> {
                       console.log(data)
                       const payload = {};
                       payload.fetchStatus = "pending";
                       payload.posts = data;
                       return payload;
                   }).then((data)=> {
                       data.fetchStatus = 'success';
                   return data} )
                    //console.log(prom)
                })
        },
        addNewPost: (uid, payload,docId) => {
            database.collection('usersPosts').doc(`${docId}`).set(
                {
                    about: {
                        descr: payload.descr,
                        title: payload.title,
                        img: payload.img,
                        postTime: firebase.firestore.FieldValue.serverTimestamp()
                    },
                    id:docId,
                    userRef: database.doc(`users/${uid}`)
                }
            );
           return  database.collection('usersPosts').doc(`${docId} [body]`).set(
                {
                    body: payload.body,
                    title: payload.title,
                    postTime: firebase.firestore.FieldValue.serverTimestamp(),
                    userRef: database.doc(`users/${uid}`),
                }
            );
        },
        editPost: (payload,docId) => {
          database.collection('usersPosts').doc(`${docId}`)
              .update({
                  "about.descr":payload.descr,
                  "about.title":payload.title,
                  "about.img":payload.img,
              })
              .catch((er)=> console.log(er.message))
            return database.collection('usersPosts').doc(`${docId} [body]`)
                .update({
                    body: payload.body,
                    title: payload.title,
                })

        },
        deletePost: (id) => {
            database.collection('usersPosts').doc(`${id} [body]`)
                .delete()
                .then(() =>{
                console.log("Document successfully deleted!");
            }).catch((error) => {
                console.error("Error removing document: ", error);
            });
              database.collection('usersPosts').doc(`${id}`)
                .delete()
                .then(() => {
                    console.log("Document successfully deleted!");
                })
                .catch((error) => {
                console.error("Error removing document: ", error);
            });
              return database.collection('deleted').doc('PfYhQTKIPkte1iOrjOYU')
                  .update({
                      deleted:firebase.firestore.FieldValue.arrayUnion(id)
                  })
                  .catch((error) => {
                      console.error("Error removing document: ", error);
                  });
        }
    }
}