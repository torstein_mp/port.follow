import {FETCH_ACTIVE, SIGN_IN, Sign_OUT, UPDATE_PROFILE} from "../AC/loginActions";

const cookie = JSON.parse(localStorage.getItem('authUser'));
const initialState = {
  user: cookie ? cookie.userData : null,
  userId:cookie ? cookie.token : null,
    fetchStatus: !!cookie,
};

export default (state = initialState,action) =>{
    switch (action.type) {
        case SIGN_IN:
            return {
                ...state,
                user: action.userData,
                userId:action.userId,
                userDB:action.userDB,
                fetchStatus:false
            };
        case  FETCH_ACTIVE:
            return {
                ...state,
                fetchStatus: true,
            };
        case UPDATE_PROFILE:
            return {
                ...state,
                user:action.userData
            };
        case Sign_OUT:
            return {
                ...state,
                user:{},
                userId:''
            };
        default:
            return state
    }
}