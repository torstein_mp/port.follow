import React, {Component, Fragment} from 'react';
import {Redirect, Route, Switch, withRouter} from "react-router-dom";
import {connect} from "react-redux";
import { WiredSpinner } from "wired-spinner"


import Header from "./components/Overall/Header";
import HomeComponent from "./components/Home/HomeComponent";
import AuthWrap from "./components/PrivateRoutes/AuthWrap.container";
import Settings from "./components/Settings/Settings.component";
import NewPost from "./components/MyPosts/NewPost.component";
import ViewPost from "./components/ViewPost.component";
import PostStorage from "./components/MyPosts/PostStorage.component";

import './style/main.scss'
import EditPost from "./components/MyPosts/EditPost.component";

class App extends Component {

    render() {
        const {status} = this.props;
        return (
            status ?
                <div className='spinner'>
                <wired-spinner  spinning duration="1000"/>
                </div>
                :
                <Fragment>
                    <Header/>
                    <Switch>

                        <Route exact path="/" component={HomeComponent}/>
                        <AuthWrap path="/settings" component={Settings}/>
                        <AuthWrap path="/myPosts" component={PostStorage}/>
                        <AuthWrap exact path="/new-post" component={NewPost}/>
                        <AuthWrap exact path="/edit-post/:id" component={EditPost}/>
                        <Route path='/post/:id' component={ViewPost}/>

                        <Redirect to='/'/>
                    </Switch>
                </Fragment>
        );
    }
}
const  mapStateToProps = (state)=>({
   status:state.login.fetchStatus
});

App.propTypes = {};

export default withRouter(connect(mapStateToProps)(App));
