import api from "../api";


export const SIGN_IN = "SIGN_IN";
export const SIGN_UP = "SIGN_UP";
export const Sign_OUT = "SIGN_OUT";
export  const UPDATE_PROFILE = "UPDATE_PROFILE";
export const  FETCH_ACTIVE  = "FETCH_ACTIVE";

  const  fetchActive = () => ({
    type:FETCH_ACTIVE,
});
export const signedIn = (user,userIndex,userDataStore)=> ({
    type: SIGN_IN,
    userData:user,
    userId: userIndex,
    userDB: userDataStore,
});
export const  updateProfile = user => ({
    type: UPDATE_PROFILE,
    userData:user
});
const signedOut = () =>({
   type:Sign_OUT,
});

export const signInUser = (email,password) => dispatch =>{
    dispatch(fetchActive());
    return api.authentication.signIn(email,password)
    .then((payload)=>{
       // console.log(user.user);
        dispatch(signedIn(payload.user,payload.id,payload.dataStore))
    })};

export const signOut = () =>  dispatch => {
  return  api.authentication.signOut()
      .then(()=>{
          dispatch(signedOut());
              console.log("SIGNED_OUT")
      })
};
